package mx.lania.vehiculos.servicios;

import mx.lania.vehiculos.entidades.Cliente;
import mx.lania.vehiculos.entidades.Vehiculo;
import mx.lania.vehiculos.repositorios.RepositorioClientes;
import mx.lania.vehiculos.repositorios.RepositorioVehiculos;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

//@Service
@Component
public class ServicioVehiculos {

    RepositorioVehiculos repoVehiculos;
    RepositorioClientes repoClientes;

    public ServicioVehiculos(RepositorioVehiculos repoVehiculos,
                             RepositorioClientes repoClientes) {
        this.repoVehiculos = repoVehiculos;
        this.repoClientes = repoClientes;
    }

    @Transactional
    public Vehiculo guardarNuevo(Vehiculo v) {
        Cliente cl = v.getCliente();
        if (cl != null && cl.getClienteId() != null) {
            Optional<Cliente> opCl = repoClientes.findById(cl.getClienteId());
            if (opCl.isPresent()) {
                // Cliente existe, solo guardamos Vehiculo
                v = repoVehiculos.save(v);
                return v;
            }
        }
        cl.setClienteId(null);
        cl = repoClientes.save(cl);
        v.setCliente(cl);
        return repoVehiculos.save(v);
    }
}
