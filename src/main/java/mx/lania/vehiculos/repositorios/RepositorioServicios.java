package mx.lania.vehiculos.repositorios;

import mx.lania.vehiculos.entidades.Servicio;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RepositorioServicios extends JpaRepository<Servicio,Integer> {
}
